Professional Plumbing is a Tasmanian owned company founded by Paul Bottomley in 1989. The business has grown since inception, supplying reliable, competitively priced residential, commercial and environmental plumbing requirements.

Today Paul and Pip Bottomley along with their dedicated Senior Management Team Todd Rayner and Andrew Downham bring more than 80 years of experience in the plumbing, construction and waste water industry.

Professional Plumbing is situated in Derwent Park, however, we offer a statewide service to our loyal and valued clients.

Address : Hobart, 91 Lampton Ave, Derwent Park, TAS 7000, Australia

Phone : +61 3 6273 0755
